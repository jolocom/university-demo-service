import App, { Container } from 'next/app';

import { appWithTranslation, i18n } from 'i18n';
import { MainHeader, LanguageSwitcher } from 'components';

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <div className="u-container row between-xs">
          <MainHeader />

          <LanguageSwitcher i18n={i18n} />
        </div>

        <Component {...pageProps} />

        <style jsx>{''}</style>
      </Container>
    );
  }
}

MyApp = appWithTranslation(MyApp);

export default MyApp;
