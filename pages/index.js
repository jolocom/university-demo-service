import { Landing, ApplyEnroll, Tooltip } from 'components';
import { withNamespaces } from 'i18n';

let Index = ({ t }) => {
  const [step, setStep] = React.useState(0);

  return (
    <div className="u-container u-margin-bottom">
      {
        [
          <Landing onSubmit={() => setStep(step + 1)} />,
          <div className="u-margin-top-lg">
            <ApplyEnroll />
          </div>,
        ][step]
      }

      <Tooltip
        triggerElement={
          <i className="fa fa-question-circle text-gray tooltip-md" />
        }
      >
        {t('common.branding.demo-info')}
      </Tooltip>

      <style jsx>{`
        .tooltip-md {
          font-size: 1.5rem;
        }
      `}</style>
    </div>
  );
};

Index = withNamespaces()(Index);

export default Index;
