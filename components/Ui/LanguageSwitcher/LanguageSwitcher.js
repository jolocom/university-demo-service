import classnames from 'classnames';

const LanguageSwitcher = ({ i18n }) => {
  const flags = {
    nl: 'https://restcountries.eu/data/bel.svg',
    en: 'https://restcountries.eu/data/gbr.svg',
  };

  return (
    <ul>
      {i18n.options.allLanguages.map(code => (
        <li
          key={code}
          className={classnames({
            active: i18n.language === code,
          })}
          onClick={() => i18n.changeLanguage(code)}
        >
          <img src={flags[code]} alt={`Flag of the ${code}`} height="12" />
          <span>{code}</span>
        </li>
      ))}

      <style jsx>{`
        ul {
          list-style: none;
          margin: 0;
          padding: 0;
        }

        ul :global(li) {
          display: inline-flex;
          align-items: center;
          padding: 8px 14px;
          margin-right: 12px;
          cursor: pointer;
          border-radius: 0px;
        }
        ul :global(li:last-child) {
          margin-right: 0;
        }
        ul :global(li:hover),
        ul :global(li.active) {
          background-color: #0064b4;
          color: #fff;
        }
        ul :global(li.active) {
          font-weight: 700;
        }
        ul :global(img) {
          width: 16px;
          margin-right: 5px;
        }
        ul :global(span) {
          text-transform: uppercase;
        }
      `}</style>
    </ul>
  );
};

export default LanguageSwitcher;
