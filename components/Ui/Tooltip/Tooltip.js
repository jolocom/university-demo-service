const Tooltip = ({ triggerElement, children }) => {
  return (
    <div className="Tooltip">
      <aside className="Tooltip__Content a-tooltip a-tooltip--top-left">
        {children}
      </aside>
      {triggerElement}

      <style jsx>{`
        .Tooltip {
          display: inline;
          position: fixed;
          right: 15px;
          bottom: 15px;
        }
        .Tooltip__Content {
          position: absolute;
          right: ${triggerElement ? '-8px' : 'initial'};
          display: ${triggerElement ? 'none' : 'initial'};
          bottom: 34px;
          width: 38rem;
          font-size: 1.25rem;
          font-style: italic;
        }
        .Tooltip:hover > .Tooltip__Content {
          display: initial;
        }
      `}</style>
    </div>
  );
};

export default Tooltip;
