import { Button } from '@acpaas-ui/react-components';

const CheckItem = ({ success, children }) => {
  return (
    <div className="CheckItem row col-xs">
      <div>
        <Button
          type={success ? 'success' : 'primary'}
          icon={success ? 'check' : 'arrow-right'}
          size="small"
          className="u-margin-right"
          style={noCursorStyle}
        />
      </div>

      <section>{children}</section>

      <style jsx>{`
        .CheckItem :global(.Button) {
          flex: 1;
        }
        section {
          flex: 10;
        }
        p {
          margin: 0;
        }
      `}</style>
    </div>
  );
};

const noCursorStyle = {
  cursor: 'auto',
};

export default CheckItem;
