import { withNamespaces } from 'i18n';

let MainHeader = ({ t }) => {
  return (
    <div>
      <figcaption className="u-text-bold">
        {t('common.branding.name')}{' '}
      </figcaption>

      <style jsx>{`
        figcaption {
          font-size: 1.25rem;
          padding: 0.75rem 0 0.5rem 0;
          white-space: pre-wrap;
        }
      `}</style>
    </div>
  );
};

MainHeader = withNamespaces()(MainHeader);

export default MainHeader;
