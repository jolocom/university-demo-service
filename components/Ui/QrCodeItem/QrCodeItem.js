import classnames from 'classnames';
import { Checkbox } from '@acpaas-ui/react-components';

const QrCodeItem = ({ note, source, success }) => {
  const renderContent = () => {
    if (success) {
      return <Checkbox checked />;
    }

    if (source) {
      return (
        <figure className="m-image">
          <img src={source} alt="figure image" />
        </figure>
      );
    }
  };

  return (
    <div className="QrCodeItem row center-xs">
      <p
        className={classnames({
          'text-gray': success || !source,
        })}
      >
        {note}
      </p>

      <div className="u-full">{renderContent()}</div>

      <style jsx>{`
        p {
          width: 13.5rem;
        }

        .QrCodeItem :global(.a-input__checkbox) {
          margin-top: 15px;
        }
        .QrCodeItem :global(.a-input__checkbox input:checked + label:before) {
          background-color: #4aa32b;
          box-shadow: inset 0 0 0 1px #4aa32b;
          transform: scale(1.5);
        }
      `}</style>
    </div>
  );
};

export default QrCodeItem;
