import { useState, useEffect } from 'react';
import { withNamespaces } from 'i18n';

import { CheckItem } from 'components';

import { getQrCode, awaitStatus } from 'utils/sockets';

let ApplyEnroll = ({ t }) => {
  const [qrCode, setQrCode] = useState(null);
  const [hasCred, setHasCred] = useState(false);
  const [acceptedCred, setAcceptedCred] = useState(false);

  useEffect(() => {
    (async() => {
      try {
        await handleAuth();
        await handleCredentialOffer();
        setQrCode(null);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  const handleAuth = async() => {
    const { qrCode: ssoQrCode, socket, identifier } = await getQrCode(
      'authenticate'
    );
    setQrCode(ssoQrCode);
    await awaitStatus({ socket, identifier });
    setHasCred(true);
  };

  const handleCredentialOffer = async() => {
    const { qrCode: applicationQrCode, socket, identifier } = await getQrCode(
      'receive',
      {
        credentialType: 'application',
        data: JSON.stringify({
          caseNumber: `${Date.now()}`.substring(4, 10),
          university: 'University of Antwerp',
          faculty: 'Data, Systems, Society',
        }),
      }
    );

    setQrCode(applicationQrCode);

    await awaitStatus({ socket, identifier });
    setAcceptedCred(true);
  };

  return (
    <div className="ApplyEnroll">
      <header>
        <h1>{t('common.apply-enroll.title')}</h1>
        <p>{t('common.apply-enroll.description')}.</p>
      </header>

      <div className="row u-margin-top-lg">
        <div className="col-xs-6">
          <CheckItem success={hasCred}>
            <p className={hasCred && 'text--success'}>
              {t('common.apply-enroll.sign-in.entry')}.
            </p>
          </CheckItem>
          {hasCred && (
            <CheckItem success={acceptedCred}>
              <p className={acceptedCred && 'text--success'}>
                {t('common.apply-enroll.sign-in.success')}.
              </p>
              <p
                className={
                  'u-margin-top-xs' + (acceptedCred ? ' text--success' : '')
                }
              >
                {t('common.apply-enroll.cred-offer.entry')}.
              </p>
            </CheckItem>
          )}
          {acceptedCred && (
            <CheckItem success={false}>
              <p>{t('common.apply-enroll.cred-offer.list-text')}</p>
              <ul>
                <li>{t('common.apply-enroll.cred-offer.list-1')}</li>
                <li>{t('common.apply-enroll.cred-offer.list-2')}</li>
                <li>{t('common.apply-enroll.cred-offer.list-3')}</li>
                <li>{t('common.apply-enroll.cred-offer.list-4')}</li>
                <li>{t('common.apply-enroll.cred-offer.list-5')}</li>
              </ul>
            </CheckItem>
          )}
        </div>
        <div className="col-xs-6">
          <figure className="m-image">
            {(!hasCred || !acceptedCred) && <img src={qrCode} alt="qrCode" />}
            <figcaption className="center-xs">
              {!hasCred && '1 / 2'}
              {hasCred && !acceptedCred && '2 / 2'}
            </figcaption>
          </figure>
        </div>
      </div>

      <style jsx>{`
        .ApplyEnroll :global(.CheckItem + .CheckItem) {
          margin-top: 2.75rem;
        }

        figcaption {
          padding: 0;
        }

        .text--success {
          color: #4aa32b;
        }
      `}</style>
    </div>
  );
};

ApplyEnroll = withNamespaces()(ApplyEnroll);

export default ApplyEnroll;
