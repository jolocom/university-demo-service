import { withNamespaces } from 'i18n';

import { Button, Breadcrumbs } from '@acpaas-ui/react-components';

let Landing = ({ onSubmit, t }) => {
  return (
    <>
      <div className="u-text-center">
        <figure className="m-image">
          <img
            src="/static/images/university-cover-image.png"
            height="312"
            alt="figure image"
          />
        </figure>
      </div>

      <div className="row middle-xs col-xs-12 u-margin-top">
        <i className="fa fa-home u-margin-right-xs" />
        <Breadcrumbs
          items={[
            { name: t('common.landing.breadcrumbs.research-institute') },
            { name: t('common.landing.breadcrumbs.faculties') },
          ]}
        />
      </div>

      <div className="u-margin-top">
        <h1>{t('common.landing.title')}</h1>

        <div className="row">
          <div className="col-sm-8 col-xs-12">
            <p>{t('common.landing.description')}</p>
          </div>
          <div className="col-sm-4 col-xs-12 center-xs top-xs">
            <Button
              type="secondary"
              size="small"
              className="u-margin-top"
              onClick={onSubmit}
            >
              {t('common.button.apply')}
            </Button>
          </div>
        </div>
      </div>
      <style jsx>{''}</style>
    </>
  );
};

Landing = withNamespaces()(Landing);

export default Landing;
