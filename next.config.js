const { parsed: localEnv } = require('dotenv').config();
const webpack = require('webpack');

module.exports = {
  distDir: 'build',
  webpack: config => {
    config.plugins.push(
      new webpack.EnvironmentPlugin(localEnv)
    );
    return config;
  },
  publicRuntimeConfig: {
    backendUrl: process.env.SERVICE_URL,
  },
};
